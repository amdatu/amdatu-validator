#!/bin/bash
#
# Run etcd with default single node config on the host network. This
# allows the integration tests to talk to localhost.

docker run -d --network=host --name=etcd quay.io/coreos/etcd:v2.3.8
