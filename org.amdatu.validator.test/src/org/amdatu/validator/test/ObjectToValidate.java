/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.validator.test;

import java.util.Optional;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ObjectToValidate {

	@NotNull
	private String m_name;

	private Optional<@Size(min=3) String> m_optionalValue;
	
	public ObjectToValidate(String name, String optionalValue) {
		m_name = name;
		setOptional(optionalValue != null ? Optional.of(optionalValue) : Optional.empty());
	}

	public String getName() {
		return m_name;
	}

	public void setName(String name) {
		m_name = name;
	}

	public Optional<String> getOptional() {
		return m_optionalValue;
	}

	public void setOptional(Optional<String> m_optionalValue) {
		this.m_optionalValue = m_optionalValue;
	}
}
