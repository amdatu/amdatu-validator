/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.validator.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.amdatu.validator.ValidatorService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ValidatorServiceTest {
	private volatile ValidatorService m_validator;
	
	
	@Before
	public void setup() {
		configure(this).add(createServiceDependency().setService(ValidatorService.class).setRequired(true)).apply();
	}
	
	@Test(expected=ConstraintViolationException.class)
	public void testInvalid() {
		m_validator.validate(new ObjectToValidate(null, null));
	}		
	
	@Test(expected=ConstraintViolationException.class)
	public void testInvalidOptional() {
		m_validator.validate(new ObjectToValidate(null, "ab"));
	}		
	
	@Test
	public void testValid() {
		m_validator.validate(new ObjectToValidate("test", null));
		m_validator.validate(new ObjectToValidate("test", "abc"));
	}
	
	@Test
	public void testGetUnderlyingValidator() {
		Validator validator = m_validator.getValidator();
		assertNotNull(validator);
	}
	
	@Test
	public void testUseUnderlyingValidator() {
		Validator validator = m_validator.getValidator();
		Set<ConstraintViolation<ObjectToValidate>> result = validator.validate(new ObjectToValidate(null, null));
		assertFalse(result.isEmpty());
		
		Set<ConstraintViolation<ObjectToValidate>> result2 = validator.validate(new ObjectToValidate("test", "abc"));
		assertTrue(result2.isEmpty());
	}
	
	@Test
	public void testValidUseHibernateSpecificConstraint() {
		final String valid_isbn = "978-161-729-045-9";
		m_validator.validate(new Book(valid_isbn));
	}
	
	@Test(expected=ConstraintViolationException.class)
	public void testInvalidUseHibernateSpecificConstraint() {
		final String invalid_isbn = "123-161-729-045-9";
		m_validator.validate(new Book(invalid_isbn));
	}
	
	@Test
	public void testDefaultErrorMessage() {
		try {
			m_validator.validate(new ObjectToValidate(null, null));
		} catch(ConstraintViolationException ex) {
			ConstraintViolation<?> violation = ex.getConstraintViolations().iterator().next();
			String message = violation.getMessage();
			assertEquals("m_name must not be null", violation.getPropertyPath() + " " + message);
		}
	}
	
	@After
	public void cleanup() {
		cleanUp(this);
	}
}
