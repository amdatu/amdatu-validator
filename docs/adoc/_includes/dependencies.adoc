== Dependencies

The following table provides an overview of the required and optional dependencies for Amdatu Validator:

[cols="40%,10%,50%"]
|===
| Bundle
	|Required
	| Description
| org.apache.felix.dependencymanager
	| yes
	| Apache Felix Dependency Manager is used internally by all Amdatu components
| javax.validation.api
	| yes
	| The Bean Validation API
| com.sun.el.javax.el
	| yes
	| Expression Language Implementation, used by Bean Validation for message interpolation
| org.hibernate.validator
	| yes
	| Hibernate implementation for Bean Validation
| jakarta.validation.jakarta.validation-api
	| yes
	| The Jakarta Bean Validation API
|===
